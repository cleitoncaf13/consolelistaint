﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleListInt
{
    class Program
    {
        static List<int> lista;
        static List<int> listaInvertida;
        static int cont = 0;


        static void Main(string[] args)
        {
            listaInvertida = new List<int>();
            lista = new List<int>
            {
                10,
                20,
                30,
                40,
                50
            };

            menu();
        }

        static void menu()
        {

            Console.WriteLine("\n**********ESCOLHA SUA AÇÃO**********\n");

            Console.WriteLine("Para Imprimir a média pressione a tecla M");

            Console.WriteLine("Para Imprimir a lista invertida pressione a tecla I");

            Console.WriteLine("Para sair pressione a tecla S");

            OpcaoSelecionada();
        }

        static void OpcaoSelecionada()
        {
            ConsoleKeyInfo cki = new ConsoleKeyInfo();
            cki = Console.ReadKey(true);

            if (cki.Key == ConsoleKey.M)
            {
                imprimirMedia();
            }
            else if (cki.Key == ConsoleKey.I)
            {
                inverterLista();
            }
            else if (cki.Key == ConsoleKey.S)
            {
                Console.Beep();
                return;
            }

            menu();
        }

        private static void inverterLista()
        {
            Inverter(lista.Count(), lista);
            Console.WriteLine("\n * *********LISTA NORMAL* *********\n");
            for (int i = 0; i < lista.Count(); i++)
            {
                Console.WriteLine("{0}", lista[i]);
            }

            Console.WriteLine("\n * *********LISTA INVERTIDA* *********\n");
            for (int i = 0; i < listaInvertida.Count(); i++)
            {
                Console.WriteLine("{0}", listaInvertida[i]);
            }

        }

        static void imprimirMedia()
        {

            int m = 0;
            int cont = 0;
            m = mediaArt(lista.Count(), lista);
            cont = contador(m, lista);
            Console.Write("\n A média é: {0} \n", m / lista.Count());
            Console.Write("\n São {0} ocorrencias de numero maior que a Média({1})\n", cont, m / lista.Count());


        }


        static int mediaArt(int pSize, List<int> list)
        {
            if (pSize == 1)
            {
                return list[0];
            }
            int Media = list[pSize - 1] + mediaArt(pSize - 1, list);
            return Media;

        }

        static void Inverter(int pSize, List<int> list)
        {
            listaInvertida.Add(list[pSize - 1]);
            if (pSize != 1)
            {
                Inverter(pSize - 1, list);
            }
        }

        static int contador(int soma, List<int> list)
        {

            for (int i = 0; i < list.Count(); i++)
            {
                int n = list[i];
                if (n >= soma / lista.Count())
                {
                    cont++;
                }
            }
            return cont;
        }
    }
}
